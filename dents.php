<?php
/**
 * Copyright 2010  Franz Keferböck keferboeck@kde.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of 
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

  $tag = 'kde';

  /** Using php4 syntax for compatibility reasons */
  class Dent {
    function Dent($text, $url, $user, $uimg, $uurl, $time, $simg) {
      $this->text = $text;
      $this->url  = $url;
      $this->user = $user;
      $this->uimg = $uimg;
      $this->uurl = $uurl;
      $this->time = $time;
      $this->simg = $simg;
    }
    var $text;
    var $url;
    var $user;
    var $uimg;
    var $uurl;
    var $time;
    var $simg;
  }

  class Answer {
    function Answer($dents, $requests) {
      $this->dents  = $dents;
      $this->requests = $requests;
    }
    var $dents;
    var $requests;
  }

  function cmp($a, $b) {
    $a_ts = strtotime($a->time);
    $b_ts = strtotime($b->time);
    if ($a_ts == $b_ts) return 0;
    return ($a_ts < $b_ts) ? -1 : 1;
  }

  if(!function_exists('_json_encode')) {
    include_once('JSON.php');
    $GLOBALS['JSON_OBJECT'] = new Services_JSON();
    function _json_encode($value) {
      return $GLOBALS['JSON_OBJECT']->encode($value); 
    }
    function _json_decode($value) {
      return $GLOBALS['JSON_OBJECT']->decode($value);
    }
  }

  $dents = array();
  $requests = array();
  date_default_timezone_set (GMT);

  /** Fetch twitter dents */
  $para = isset($_GET['twitter']) ? str_replace('#', '%23', $_GET['twitter']) : "?q=$tag";
  $json = file_get_contents('http://search.twitter.com/search.json'.$para);
  $twitter = json_decode($json);
  if (isset($twitter)) {
    foreach($twitter->results as $r) {
      array_push($dents, new Dent(htmlspecialchars($r->text), "http://twitter.com/$r->from_user", $r->from_user, $r->profile_image_url, "http://twitter.com/$r->from_user", $r->created_at, 'images/feeds/twitter.jpg'));
    }
    $requests['twitter'] = urlencode($twitter->refresh_url);
  }

  /** Fetch identi.ca dents */
  $para = isset($_GET['identica']) ? str_replace('#', '%23', $_GET['identica']) : "?q=$tag";
  $json = file_get_contents('http://identi.ca/api/search.json'.$para);
  $identica = json_decode($json);
  if (isset($identica)) {
    $maxid = str_replace('&q=%23kde', '', str_replace('?since_id=', '', $_GET['identica']));
    foreach($identica->results as $r) {
      if ($r->id <= $maxid) continue;
      array_push($dents, new Dent(htmlspecialchars($r->text), "http://identi.ca/$r->from_user", $r->from_user, $r->profile_image_url, "http://identi.ca/$r->from_user", $r->created_at, 'images/feeds/statusnet.jpg'));
    }
    $requests['identica'] = urlencode($identica->refresh_url);
  }

  /** Fetch flickr dents */
  $para = "?tags=$tag&lang=en-us&format=json&nojsoncallback=1";
  $json = file_get_contents('http://api.flickr.com/services/feeds/photos_public.gne'.$para);
  $flickr = _json_decode($json);
  if (isset($flickr)) {
    $maxid = $_GET['flickr'];
    foreach($flickr->items as $i) {
      if (isset($maxid) && $i->published <= $maxid) continue;
      $timestr = date(DATE_RFC2822, strtotime($i->published));
      array_push($dents, new Dent(htmlspecialchars($i->title), $i->link, $i->author, $i->media->m, "http://www.flickr.com/people/$i->author_id", $timestr, 'images/feeds/flickr.gif'));
    }
    $requests['flickr'] = urlencode($flickr->modified);
  }

  /** Fetch picasa dents */
  $para = "?kind=photo&q=$tag&alt=json&max-results=25&prettyprint=true";
  $json = file_get_contents('http://picasaweb.google.com/data/feed/api/all'.$para);
  $picasa = json_decode($json);
  if (isset($picasa)) {
    $maxid = $_GET['picasa'];
    foreach($picasa->feed->entry as $e) {
      if (isset($maxid) && $e->published->{'$t'} <= $maxid) continue;
      $timestr = date(DATE_RFC2822, strtotime($e->published->{'$t'}));
      $author = '';
      foreach ($e->author as $a) $author .= ' '.$a->name->{'$t'};
      array_push($dents, new Dent(htmlspecialchars($e->summary->{'$t'}), $e->content->src, $author, $e->{'media$group'}->{'media$thumbnail'}[0]->url, $e->author[0]->uri->{'$t'}, $timestr, 'images/feeds/picasa.jpg'));
    }
    $requests['picasa'] = urlencode($picasa->feed->updated->{'$t'});
  }

  /** Fetch youtube dents */
  $para = "?alt=json&q=$tag&prettyprint=true";
  $json = file_get_contents('http://gdata.youtube.com/feeds/api/videos'.$para);
  $youtube = json_decode($json);
  if (isset($youtube)) {
    $maxid = $_GET['youtube'];
    foreach($youtube->feed->entry as $e) {
      if (isset($maxid) && $e->published->{'$t'} <= $maxid) continue;
      $timestr = date(DATE_RFC2822, strtotime($e->published->{'$t'}));
      $author = '';
      foreach ($e->author as $a) $author .= ' '.$a->name->{'$t'};
      array_push($dents, new Dent(htmlspecialchars($e->title->{'$t'}.' - '.$e->content->{'$t'}), urlencode($e->link[0]->href), $author, $e->{'media$group'}->{'media$thumbnail'}[0]->url, "http://www.youtube.com/user/".$e->author[0]->name->{'$t'}, $timestr, 'images/feeds/youtube.png'));
    }
    $requests['youtube'] = urlencode($youtube->feed->updated->{'$t'});
  }

  /** Sort dents by time */
  usort($dents, 'cmp');

  /** Write JSON */
  echo json_encode(new Answer($dents, $requests));
?>

