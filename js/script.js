/**
 * Copyright 2010  Franz Keferböck keferboeck@kde.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of 
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

var messages = new Array();
function displayDents() {
  if (messages.length > 0) {
    /* Get next dent from the list and remove it from there */
    var msg = messages[0];
    messages.erase(msg);
    /* This is for visual reasons only to alter the background in the dents list */
    odd = (odd + 1) % 2;
    /* Create new dent item and inject it into the page */
    var newItem = new Element('div', {
      'class': (odd == 1 ? 'item odd' : 'item'),
      'html': '<a href="'+msg.uurl+'"><img src="'+msg.uimg+'" alt="'+msg.user+'" /></a>'
        + '<p class="desc"><a href="'+msg.uurl+'">'+msg.user+'</a>: '+msg.text+'</p>'
        + '<p class="time" style="background: url('+msg.simg+') no-repeat top left;">'+msg.time+'&nbsp;<a href="'+msg.url+'">more...</a></p>',
      'styles': {
        'position': 'absolute',
        'top': 0
      }
    });
    newItem.fx = new Fx.Morph(newItem, {
      duration: '1500',
      transition: Fx.Transitions.Elastic.easeOut
    });
    newItem.inject('description', 'after');
    /* Animation to make space for the new item */
    var fx = new Fx.Morph($$('div.item')[1], {
      duration: 'short',
      transition: Fx.Transitions.Cubic.easeIn
    });
    fx.start({'margin-top': newItem.offsetHeight });
    /* Let the item drop down from the top and fix it in the layout */
    newItem.fx.start({'top': $('head').offsetHeight + $('description').offsetHeight + 5 /* padding-top of #main */}).chain(function() {
      newItem.setStyles({'position': 'relative', 'top': 0});
      $$('div.item')[1].setStyle('margin-top', 0);
      displayDents();
    });
  } else {
    window.setTimeout('displayDents()', 30000);
  }
}

function fetchDents() {
  var jsonRequest = new Request.JSON({
    url: "dents.php",
    method: 'get',
    onSuccess: function(json, txt) {
      if (!json) { return; }
      messages.extend(json.dents);
      resend = '';
      for (var i in json.requests) { resend += i + "=" + json.requests[i] + "&"; }
      window.setTimeout('fetchDents()', 60000);
    },
    onFailure: function(xhr) { window.status = 'An error occured while connecting to' + this.url; }
  }).send({ method: 'get', data: resend});
}

function toggleInfo() {
  var desc = $('description');
  if (!desc.fx) {
    desc.fx = new Fx.Morph(desc, {duration: '700'});
  }
  if (desc.offsetHeight == 0) {
    desc.fx.start({display: 'block'}).chain(function() { desc.fx.start({opacity: 1}); });
    $('toggleInfo').firstChild.innerHTML = '<img src="images/arrow-up.png" alt="Hide Info" width="22" height="22">Hide Info';
  } else {
    desc.fx.start({'opacity': 0}).chain(function() { desc.fx.start({display: 'none'}); });
    $('toggleInfo').firstChild.innerHTML = '<img src="images/arrow-down.png" alt="Hide Info" width="22" height="22">Show Info';
  }
}

window.addEvent('domready', function() {
  /* JS Works, so we can show the "toggle" button! */
  $('toggleInfo').style.display = 'block';
  /* fetch Information from denting platforms */
  fetchDents();
  /* Start loop to display dents in the queue */ 
  displayDents();
});

