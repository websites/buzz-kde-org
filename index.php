<?php
/**
 * Copyright 2010  Franz Keferböck keferboeck@kde.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of 
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

  if(!function_exists('json_encode')) {
    include_once('JSON.php');
    $GLOBALS['JSON_OBJECT'] = new Services_JSON();
    function json_encode($value) {
      return $GLOBALS['JSON_OBJECT']->encode($value); 
    }
    function json_decode($value) {
      return $GLOBALS['JSON_OBJECT']->decode($value);
    }
  }
  $dent_script = $_SERVER['SERVER_NAME'].str_replace('index.php', 'dents.php', $_SERVER['SCRIPT_NAME']);
  $dents = json_decode(file_get_contents("http://".$dent_script));
  $odd = 0;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="icon" type="image/png" href="favicon.ico" />
    <script type="text/javascript" src="js/mootools-1.2.4-core-yc.js"></script>
    <script type="text/javascript">
      <!--
      var odd = 1;
      var resend = '<?php
        foreach($dents->requests as $rk => $rv) {
          echo $rk.'='.$rv.'&'; }
        ?>';
      //-->
    </script>
    <script type="text/javascript" src="js/script.js"></script>
    <link rel="stylesheet" href="css/style.css" type="text/css" title="Fixed Width" />
    <title>KDE Microblogging Lifestream</title>
  </head>
  <body>
    <div id="head">
      <a href="http://www.kde.org/"></a>
      <ul>
        <li><a href="http://www.kde.org/"><img src="images/kde.png" alt="KDE Home" />KDE Home</a></li>
        <li><a href="http://planetkde.org/"><img src="images/planet.png" alt="Planet KDE" width="22" height="22" />Planet KDE</a></li>
        <li><a href="http://dot.kde.org"><img src="images/dot-news.png" alt="KDE Dot News" width="22" height="22" />KDE Dot News</a></li>
        <li><a href="http://forum.kde.org"><img src="images/forum.png" alt="KDE Forum" width="22" height="22" />KDE Forum</a></li>
        <li id="toggleInfo"><a href="javascript:toggleInfo();"><img src="images/arrow-up.png" alt="Hide Info" width="22" height="22" />Hide Info</a></li>
      </ul>
    </div>
    <div id="main">
      <div id="description">
        buzz.kde.org aggregates what people say about KDE all around the web. It currently monitors <a href="http://identi.ca">identi.ca</a>,
        <a href="http://twitter.com">twitter</a>, <a href="http://www.youtube.com">youtube</a>, <a href="http://picasaweb.google.com">Picasa Web Albums</a>
        and <a href="http://www.flickr.com">flickr</a> to show you news, opinions and other interesting stuff concerning KDE. Content is not
        filtered and nearly real time. Enjoy!
      </div>
<?php $dents->dents = array_reverse($dents->dents);
      foreach($dents->dents as $d) { ?>      <div class="item<?php echo (++$odd%2 == 1 ? ' odd' : ''); ?>">
        <a href="<?=$d->uurl?>"><img src="<?=$d->uimg?>" alt="<?=$d->user?>" /></a>
        <p class="desc"><a href="<?=$d->uurl?>"><?=$d->user?></a>: <?=$d->text?></p>
        <p class="time" style="background: url(<?=$d->simg?>) no-repeat top left;"><?=$d->time?>&nbsp;<a href="<?=$d->url?>">more...</a></p>
      </div>
<?php } ?>
    </div>
    <div id="foot">
      This stream is made from blogs, tweets, dents and flickr, youtube and picasaweb uploads of people talking about KDE. The opinions it contains
      are those of the contributor.
    </div>
  </body>
</html>

